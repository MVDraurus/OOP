#pragma once

#include <iostream>

class MyClass
{
public:
	MyClass();
	~MyClass();
	unsigned int factorial(unsigned int i);
};

