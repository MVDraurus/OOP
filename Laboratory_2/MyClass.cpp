#include "MyClass.h"
#include "pch.h"

MyClass::MyClass()
{
	std::cout << "myClass created!\n" << std::endl;
}

MyClass::~MyClass()
{
	std::cout << "myClass destroyed!\n" << std::endl;
}

unsigned int MyClass::factorial(unsigned int i)
{
	int res = i;
	while (i > 1)
		res *= --i;
	return res;
}