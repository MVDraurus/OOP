#pragma once
class DF
{
private:
	int *mas,
		raz_mas;

public:
	DF();
	DF(int);
	virtual ~DF();

	void SetMas();
	void PrintMas();

	friend int SumMas(DF *, int, int);
	friend int ProizMas(DF &, int, int);
};

