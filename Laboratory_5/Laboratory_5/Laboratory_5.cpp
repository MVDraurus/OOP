// Вариант №5
// 1. Провести модифікацію програми таким чином, щоб користувач програми міг задавати довільний розмір масиву. OK
// 2. Провести модифікацію дружніх функцій так, щоб можна було задавати довільні межі для обчислення суми та добутку елементів масиву. OK
// 3. У дружніх функціях передбачити перевірку введених даних на коректність(якщо дані повинні виходять із заданого діапазону, про це 
//    видається відповідне повідомлення і або завершення роботи програми, або повернення до вводу даних). OK
// 4. Зберегти проект, протестувати програму для різних значень вхідних даних

//#define NDEBUG

#include "pch.h"
#include "DF.h"
#include <assert.h>
#include <iostream>

using namespace std;

/**
 * @brief Функция ввода и проверки коректности ввода.
 *  Являеться обёрткой для упрощения ввода чесел,
 *  в случае если ввода строки заместь числа, выведет
 *  сообщение и попросит повторить ввод.
 *
 * @tparam T Стандартный тип даных, не рекомендуеться
 *  использовать строки.
 * @param value Переменая с для ввода
 * @param message Сообщение в случае ошибки
 */
template <typename T>
void input_check(T &value, const char *message)
{
	while (!(std::cin >> value))
	{
		std::cin.clear();
		while (getchar() != '\n');

		std::cout << message << std::endl;
	}

	while (getchar() != '\n');
}

int SumMas(DF *Obj, int n1, int n2)
{
	assert(!((n1 < 0 || n1 >= Obj->raz_mas) || (n2 < 0 || n2 >= Obj->raz_mas)));

	int sum_mas(0);
	for (int i = n1; i <= n2; i++)
		sum_mas += Obj->mas[i];

	cout << "\tOtrabotala SumMas. sum_mas=" << sum_mas << endl;
	return sum_mas;
}

int ProizMas(DF &Obj2, int n1, int n2)
{
	assert(!((n1 < 0 || n1 >= Obj2.raz_mas) || (n2 < 0 || n2 >= Obj2.raz_mas)));

	int proiz_mas(1);
	for (int i = n1; i <= n2; i++)
		proiz_mas *= Obj2.mas[i];

	cout << "\tOtrabotala ProizMas. proiz_mas=" << proiz_mas << endl;

	return proiz_mas;
}

int main(int argc, char* argv[])
{
	int S1, P1, size, boundary_first, boundary_second;
	DF *Ob1;

	do
	{
		cout << "Enter size array /> ";
		input_check(size, "Error: Enter size array /> ");
	} while (size <= 0);

	Ob1 = new DF(size);

	Ob1->SetMas();
	Ob1->PrintMas();

	cout << "Enter first boundary array /> ";
	input_check(boundary_first, "Error: Enter first boundary array /> ");

	cout << "Enter second boundary array /> ";
	input_check(boundary_second, "Error: Enter second boundary array /> ");

	S1 = SumMas(Ob1, boundary_first, boundary_second);
	cout << "\tS1=" << S1 << endl;


	cout << "Enter first boundary array /> ";
	input_check(boundary_first, "Error: Enter first boundary array /> ");

	cout << "Enter second boundary array /> ";
	input_check(boundary_second, "Error: Enter second boundary array /> ");

	P1 = ProizMas(*Ob1, boundary_first, boundary_second);
	cout << "\tP1=" << P1 << endl;

	getchar();
	return EXIT_SUCCESS;
}