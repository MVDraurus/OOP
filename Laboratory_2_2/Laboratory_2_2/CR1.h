#pragma once
class CR1
{
public:

	/**
	 * @fn	float CR1::metod1(size_t N)
	 *
	 * @brief	���������� ������� �����, ������� 3,
	 *		�� 1 �� N �� ���� ������
	 *
	 * @param	N	����� ����� ��������.
	 *
	 * @return	������� ����� � �������� [1 - N].
	 */
	float metod1(size_t N);

	/**
	 * @fn	auto CR1::metod2(float x);
	 *
	 * @brief    ���������� �������� ������� 2sin(x)*(x - 2)
	 *
	 * @param	x	�������� �������.
	 *
	 * @return	��������� ��������� �������.
	 */
	float metod2(float x);

};

