#include "pch.h"
#include "CR1.h"

float CR1::metod1(size_t N)
{
	float sum = 0.0f,
		dob = 1.0f;

	for (size_t i(1); i <= N; i++) // ���� ������
		if (i % 2 == 0)
			sum += i;

	if (sum == 0)
	{
		std::cout << "The are no pair numbers in the range!" << std::endl;
		return 0.0f;
	}

	for (size_t i(1); i <= N; i++) // ������� ������� 3
		if (i % 3 == 0)
			dob *= i;

	if (dob == 1)
	{
		std::cout << "The are no multiples of three!" << std::endl;
		return 0.0f;
	}

	return dob * sum;
}

float CR1::metod2(float x)
{
	return 2*sin(x)*(x - 2);
}