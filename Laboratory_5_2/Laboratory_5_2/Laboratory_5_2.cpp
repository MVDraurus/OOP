// Laboratory_5_2.cpp : Вариант №5

#define NDEBUG

#include "pch.h"
#include "XYZ.h"
#include <iostream>

using namespace std;

//typedef float T;

/***
 * @breaf Функция расчитует уровнение.
 *
 * @param &XYZ Указатель на объект класа
 */
void run(XYZ &xyz);

/***
 * @breaf Выводит результат.
 *
 * @param &XYZ Указатель на объект класа
 */
void print(XYZ &xyz);

/**
 * @brief Функция ввода и проверки коректности ввода.
 *  Являеться обёрткой для упрощения ввода чесел,
 *  в случае если ввода строки заместь числа, выведет
 *  сообщение и попросит повторить ввод.
 *
 * @tparam T Стандартный тип даных, не рекомендуеться
 *  использовать строки.
 * @param value Переменая для ввода
 * @param message Сообщение в случае ошибки
 */
template <typename T>
void input_check(T &value, const char *message)
{
	while (!(std::cin >> value))
	{
		std::cin.clear();
		while (getchar() != '\n');

		std::cout << message;
	}

	while (getchar() != '\n');
}

int main()
{
	XYZ xyz(-15.246, 4.642*powf(10, -2), 20.001* powf(10, 2)),
		*pxyz = new XYZ();
	T x, y, z;

	cout << "Static object :>" << endl;
	run(xyz);
	print(xyz);

	cout << endl << "Dynamic object :>" << endl
		<< '\t' << "Enter x /> ";
	input_check(x, "\tError: Enter x /> ");
	pxyz->set_x(x);

	cout << '\t' << "Enter y /> ";
	input_check(y, "\tError: Enter y /> ");
	pxyz->set_y(y);

	cout << '\t' << "Enter z /> ";
	input_check(z, "\tError: Enter z /> ");
	pxyz->set_z(z);

	run(*pxyz);
	print(*pxyz);

	getchar();
	return EXIT_SUCCESS;
}

void run(XYZ &xyz)
{
	xyz.result = (logf(powf(xyz.y, -powf(fabsf(xyz.x), (float)1 / 2))) * (xyz.x - (xyz.y / 2))) + (sinf(atanf(xyz.z))*sinf(atanf(xyz.z)));
}

void print(XYZ &xyz)
{
	cout << "Result => " << xyz.result << endl;
}