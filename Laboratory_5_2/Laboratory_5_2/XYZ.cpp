#include "pch.h"
#include "XYZ.h"
#include <assert.h>

XYZ::XYZ()
{
	this->x = *(new T());
	this->y = *(new T());
	this->z = *(new T());
}

XYZ::XYZ(T x, T y, T z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}

void XYZ::set_x(T x)
{
	assert(x != 0);

	this->x = x;
}

void XYZ::set_y(T y)
{
	assert(y >= 0 );

	this->y = y;
}

void XYZ::set_z(T z)
{
	assert(z != 0);

	this->z = z;
}
