/**
 * @file	Bread.h.
 *
 * @brief	Declares the bread class
 */

#pragma once

/**
 * @class	Bread
 *
 * @brief	����� �������� �������� �������� ���� ������� �������.
 */
class Bread
{
public:

	/**
	 * @fn	Bread::Bread();
	 *
	 * @brief	Default constructor
	 */
	Bread();

private:
	/** @brief	��� */
	std::string name;
	/** @brief	���� */
	float price;
	/** @brief	��� */
	float weight;
public:

	/**
	 * @fn	void Bread::setName(std::string name);
	 *
	 * @brief	������������� ��� ��������
	 *
	 * @param	name	��������������� ���
	 */
	void setName(std::string name);

	/**
	 * @fn	std::string Bread::getName();
	 *
	 * @brief	��������� ��� ��������.
	 *
	 * @return	std::string ��� ��������
	 */
	std::string getName();

	/**
	 * @fn	void Bread::setPrice(float price = 0.0f);
	 *
	 * @brief	������������� ���� ��������
	 *
	 * @param	price	(Optional) ���� ��������.
	 */
	void setPrice(float price = 0.0f);

	/**
	 * @fn	float Bread::getPrice();
	 *
	 * @brief	������ ���� ��������
	 *
	 * @return	float ���� ��������  
	 */
	float getPrice();

	/**
	 * @fn	void Bread::setWeight(float weight = 0.0f);
	 *
	 * @brief	������������� ��� ��������
	 *
	 * @param	weight	(Optional) ��� ��������.
	 */
	void setWeight(float weight = 0.0f);

	/**
	 * @fn	float Bread::getWeight();
	 *
	 * @brief	��������� ��� ��������.
	 *
	 * @return  float ��� ��������.
	 */
	float getWeight();
};

