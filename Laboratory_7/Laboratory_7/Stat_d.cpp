#include "pch.h"
#include "Stat_d.h"

Stat_d::Stat_d() : m_x(0), m_y(0)
{
}

Stat_d::Stat_d(int x, int y) : m_x(x), m_y(y)
{
}

Stat_d::~Stat_d()
{
}

void Stat_d::setB(int _b)
{
	B = _b;
}

int Stat_d::getB()
{
	return B;
}
