﻿#include "pch.h"

using namespace std;

double Stat_d::A = 2.75;
int Stat_d::B = 777;
int Stat_m::objAmount;

int main()
{
	do
	{
		system("cls");
		cout << "\tMenu" << endl
			<< "1. Task one" << endl
			<< "2. Task two" << endl
			<< "3. Exit" << endl
			<< "/> ";

		switch (getchar())
		{
		case '1':
		{
			Stat_d a(10, 10), b(-10, -10);

			cout << "a\t" << a.m_x << '\t' << a.m_y << endl
				<< "b\t" << b.m_x << '\t' << b.m_y << endl
				<< "for a and b static A =\t" << Stat_d::A << endl;

			cout << "for a static B =\t" << a.getB() << endl
				<< "for b static B =\t" << b.getB() << endl;

			Stat_d::A = 1.2e308;

			cout << "after change A =\t" << Stat_d::A << endl;
			a.setB(100);
			cout << "after change in a b.B =\t" << b.getB() << endl;
			b.setB(-100);
			cout << "after change in b a.B =\t" << a.getB() << endl;

			break;
		}
		case '2':
		{
			Stat_m::initAmount(0);
			Stat_m o1("St. Petersburg"), o2("Yalta");
			Stat_m o[12], o3;
			Stat_m* p = new Stat_m[25];

			cout << Stat_m::getAmount() << endl;
			delete[] p;
			cout << Stat_m::getAmount() << endl;
			break;
		}
		case '3':
			return EXIT_SUCCESS;
		default:
			break;
		}

		while (getchar() != '\n')
			continue;
		getchar();
	} while (true);
}