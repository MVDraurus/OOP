#pragma once
class Stat_m
{
public:
	Stat_m(const char*);
	Stat_m();
	virtual	~Stat_m();

	static void initAmount(const int);
	static int getAmount();

private:
	static int incObj();
	static int objAmount;
	int lenS();

	char m_s[40];
};

