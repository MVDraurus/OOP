#pragma once
class D
{
	int* m_p,
		m_n;
public:
	D(int = 10);
	D(D&);
	~D();
	int getN(void) const;

	int& operator [] (const unsigned int) const;
	D& operator = (D&);
	D operator + (const D&) const;
	D operator + (const int&) const;
	D operator - (const D&) const;
	
	friend void view(const D&);
	friend D operator*(const D&, const float&);
};
