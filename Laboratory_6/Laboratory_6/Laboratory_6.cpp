// Laboratory_6.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"

using namespace std;

void view(const D&);
D operator * (const D&, const float&);


/**
 * @brief Функция ввода и проверки коректности ввода.
 *  Являеться обёрткой для упрощения ввода чесел,
 *  в случае если ввода строки заместь числа, выведет
 *  сообщение и попросит повторить ввод.
 *
 * @tparam T Стандартный тип даных, не рекомендуеться
 *  использовать строки.
 * @param value Переменая для ввода
 * @param message Сообщение в случае ошибки
 */
template <typename T>
void input_check(T &value, const char *message)
{
	while (!(std::cin >> value))
	{
		std::cin.clear();
		while (getchar() != '\n');

		std::cout << message;
	}

	while (getchar() != '\n');
}

int main(int argc, char* argv[])
{
	D *o1, *o2, *o3, res;
	int x(5), size;

	do
	{
		cout << "Enter size o1 /> ";
		input_check(size, "Error: Enter size o1 /> ");
	} while (size <= 0 );
	o1 = new D(size);

	cout << "Fill object o1:\n";
	for (int i = 0; i < o1->getN(); i++)
	{
		cout << "\tEnter o1[" << i << "] /> ";
		cin >> (*o1)[i];
	}
	getchar();

	do
	{
		cout << "Enter size o2 /> ";
		input_check(size, "Error: Enter size o2 /> ");
	} while (size <= 0);
	o2 = new D(size);

	cout << "Fill object o2:\n";
	for (int i = 0; i < o2->getN(); i++)
	{
		cout << "\tEnter o1[" << i << "] /> ";
		cin >> (*o2)[i];
	}
	getchar();

	do
	{
		cout << "Enter size o3 /> ";
		input_check(size, "Error: Enter size o3 /> ");
	} while (size <= 0);
	o3 = new D(size);

	cout << "Fill object o3:\n";
	for (int i = 0; i < o3->getN(); i++)
	{
		cout << "\tEnter o1[" << i << "] /> ";
		cin >> (*o3)[i];
	}
	getchar();

	cout << "\no1:\t\t";	view(*o1);
	cout << "o2:\t\t"; view(*o2);

	res = (D&)(*o1 + *o2);
	cout << "\nres = o1 + o2:\t"; view(res);

	cout << "\no3:\t\t";	 view(*o3);
	res = (D&)(*o3 + x);
	cout << "res = o3 + 5:\t"; view(res);

	res = (D&)(*o3 - *o2);
	cout << "\nres = o3 - o2:\t"; view(res);
	res = (D&)(*o2 - *o3);
	cout << "res = o2 - o3:\t";	view(res);

	cout << "\no3:\t\t";	 view(*o3);
	res = (D&)(*o3 * x);
	cout << "res = o3 * 5:\t"; view(res);

	cout << "\no1:\t\t";		view(*o1);
	cout << "o2:\t\t";		view(*o2);
	cout << "o3:\t\t";		view(*o3);

	getchar();
	return 0;
}

// функция вывода состояния объектов типа C
void view(const D& o)
{
	for (int i = 0; i < o.m_n; i++)
		cout << o[i] << '\t';

	cout << endl;
}

D operator * (const D& d, const float& num)
{
	D tmp(d.m_n);

	for (int i = 0; i < tmp.m_n; i++)
		tmp.m_p[i] = d.m_p[i] * num;

	return tmp;
}