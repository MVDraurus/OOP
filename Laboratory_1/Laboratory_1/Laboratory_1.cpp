﻿#include "pch.h"
#include <iostream>
#include <conio.h>
#include <stdlib.h>

using namespace std;

void fun1();
void fun2();
void fun3();
void fun4();
void fun5();

int main()
{
	setlocale(LC_ALL, "RUS");
	int x, sum = 0;
	bool flag = true;
	while (flag)
	{
		cout <<"Enter x -> ";
		cin >> x;
		if (x > 0)
		{
			sum += x;
			continue;
		}
		flag = false;
	}
	cout << sum << endl;
	cout << " <<< Виполнение первой ф-ции >>> " << endl;
	fun1();
	cout << " <<< Виполнение второй ф-ции >>> " << endl ;
	fun2();
	cout << " <<< Виполнение третей ф-ции >>> " << endl ;
	fun3();
	cout << " <<< Виполнение четвертой ф-ции >>> " << endl;
	fun4();
	cout << endl <<  " <<< Виполнение пятой ф-ции >>> " << endl ;
	fun5();
	_getch();
	return 0;
}

void fun1()
{
	//system("cls");
	int x = 5;
	int * px = &x;
	cout << "Value x\t\t" << x << endl;
	cout << "Address x\t" << &x << endl;
	cout << "Pointer px\t" << px << endl;
	_getch();

}

void fun2()
{
	//system("cls");
	double v1 = 0.05, v2 = 2.5e32;
	double * pv;
	pv = &v1;
	cout << "pv =\t" << pv << endl;
	cout << "v1 =\t" << v1 << endl;
	cout << "*pv = \t";
	cout << *pv << endl;
	pv = &v2;
	cout << "pv =\t" << pv << endl;
	cout << "v2 =\t" << v2 << endl;
	cout << "*pv = \t";
	cout << *pv << endl;
	_getch();
}

void fun3()
{
	//system("cls");
	int* pi; unsigned short* pu;
	pi = new int;
	pu = new unsigned short(200);
	double* pd = new double;
	float* pf = new float(-3.15);
	cout << "Pointers";
	cout << "\npi\t" << pi << "\npu\t" << pu;
	cout << "\npd\t" << pd << "\npf\t" << pf;
	cout << "\nValues with initialization";
	cout << "\n* pu\t" << *pu;
	cout << "\n* pf\t" << *pf;
	cout << "\nValues without initialization";
	cout << "\n* pi\t" << *pi;
	cout << "\n* pd\t" << *pd;
	*pi = -*pu; *pd = -*pf;
	cout << "\nValues after initialization";
	cout << "\n* pi\t" << *pi;
	cout << "\n* pd\t" << *pd << endl;
	_getch();
}

void fun4()
{
	//system("cls");
	int i;
	int array[] = { 10, 20, 30, 40, 50 };
	int size = sizeof(array) / sizeof(array[0]);
	for (i = 0; i < size; i++)
		cout << *(array + i) << '\t';
	cout << endl;
	int* p = array;
	int count = 0;
	do
	{
		cout << *p++ << '\t';
		count++;
	} while (count < size);
	cout << endl;
	int sum = 0;
	for (int i1 = 0; i1 < size; i1++)
		sum += array[i1];
	cout << "Sum of elements in array = " << sum;
	_getch();
}
void fun5()
{
	struct Address {
		char city[20]; char street[30]; int house;
	};
	struct Person {
		char Fname[15]; char Lname[20];
	};
	// иерархическая структура
	struct Employee {
		Person p;		// вложенная структура
		Address addr;		// вложенная структура
	};

	//system("cls");
	Employee *p = new Employee;
	cout <<"Enter first name: ";
	cin >> p->p.Fname;
	cout   << "Enter last name: ";
	cin >> p->p.Lname;
	cout   << "Enter the name of city: ";
	cin >> p->addr.city;
	cout   << "Enter your street adress: ";
	cin >> p->addr.street;
	cout   << "Enter your house adress: ";
	cin >> p->addr.house;
	cout << endl << "Entered data";
	cout << endl << p->p.Fname;
	cout << endl << p->p.Lname;
	cout << endl << p->addr.city;
	cout << endl << p->addr.street;
	cout << endl << p->addr.house;
	delete p;
	_getch();
}
